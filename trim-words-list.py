#!/usr/bin/env python3

import argparse
import re


# Set up argparse
p = argparse.ArgumentParser(description="Solving the NYT game 'Letter Boxed': https://www.nytimes.com/puzzles/letter-boxed", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
p.add_argument('-i', '--input-list', type=str, default="words-list.txt", help="Initial dictionary filename", metavar="words-list.txt")
p.add_argument('-o', '--output-list', type=str, help="Output dictionary filename", metavar="words-list-trimmed.txt")
args = p.parse_args()

output_file = args.output_list if args.output_list else args.input_list[:-4]+"-trimmed"+args.input_list[-4:]

# Matches words at least 3 letters long that have no consecutive repeated letters
re_match = re.compile(r"^(?:([a-z])(?!\1)){3,}$", re.IGNORECASE)

with open(args.input_list) as f_in, open(output_file, 'w') as f_out:
  f_out.write("".join(line for line in f_in if line[0].lower() == line[0] and re_match.match(line)))
