#!/usr/bin/env python3

import argparse
import re

# Set up argparse
p = argparse.ArgumentParser(description="Solver for 'Letter Boxed': https://www.nytimes.com/puzzles/letter-boxed",
                            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
p.add_argument('sides', type=str.upper, nargs=4, help="List of the letters in each side", metavar="ABC")
p.add_argument('-n', '--number', type=int, default=5, help="Number of words allowed in the chain", metavar="#")
p.add_argument('-l', '--words-list', type=str, default="words-list.txt",
               help="Text file containing line-separated list of words", metavar="words-list.txt")
p.add_argument('-t', '--type', type=str.lower, default="words", help="Solution based on fewest (words) or (letters)",
               metavar="type")
args = p.parse_args()

# Pass over word list getting words that match the correct side-alternating pattern
all_letters = "".join(args.sides)
re_sides = re.compile("^[" + all_letters + "]{3,}$", re.IGNORECASE)
re_consecutive = re.compile("|".join(f"[{side}]{{2}}" for side in args.sides), re.IGNORECASE)
with open(args.words_list) as f:
  words_list = set(line.strip().upper() for line in f if re_sides.match(line) and not re_consecutive.search(line))

# Make dict for distinct letter sets for each starting letter:
# {'A': {{'a', 'r', 'm', 'd'}: 'armada', {'a', 'b', 's', 'o', 'r'}: 'absorb', ...}, 'B': {...}, ...}
starting_letters_dict = dict((starting_letter, dict()) for starting_letter in all_letters)
for word in words_list:
  if not starting_letters_dict[word[0]].get(frozenset(word)):
    starting_letters_dict[word[0]][frozenset(word)] = word

# Now go over all the words, building up a word chain from the ending/starting letters
# Because of frozensets, words are added only if they add unused letters, and checking the solution is fast
solution_set = frozenset(all_letters)
solution_chains = list()
for word in words_list:
  word_chain = {frozenset(word): (word,)}
  while len(next(iter(word_chain.values()))[0]) < args.number and not word_chain.get(solution_set):
    word_chain = {frozenset(chain_set.union(word)): chain+(word,)
                  for chain_set, chain in word_chain.items()
                  for word in (word for word_set, word in starting_letters_dict[chain[-1][-1]].items()
                               if not word_set < chain_set)}
  if word_chain.get(solution_set):
      solution_chains.append(word_chain.get(solution_set))

# Sort the solutions list and print
match args.type:
  case "words":  # gets solution with fewest words (then fewest letters)
    solution_sort = lambda x: (len(x), len("".join(x)))
  case "letters":  # gets solution with fewest letters (then fewest words)
    solution_sort = lambda x: (len("".join(x)), len(x))
  case _:
    solution_sort = lambda x: x
print(f"Solution: {sorted(solution_chains, key=solution_sort)[0]}")
